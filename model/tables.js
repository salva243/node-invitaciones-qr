require('dotenv').config();
const mysql = require('mysql2');

const connection = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    port: process.env.DB_PORT
});

const query = (query, callback) => {
    connection.query(query, (error, result, fields) => {
        if (error){
            callback(error, null);
        }else{
            callback(null, result);
        };
    });
}

module.exports = {
    query
}