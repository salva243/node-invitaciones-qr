const eventos = `
    CREATE TABLE IF NOT EXISTS eventos (
        id INT PRIMARY KEY AUTO_INCREMENT,
        nombre_evento TEXT DEFAULT NULL,
        numero_mesas INT DEFAULT 0,
        numero_sillas_x_mesa INT DEFAULT 0,
        estatus INT DEFAULT 1
    )
`;
/**
 * Escaneada: 0 = no, 1 = si 
 */
const invitaciones = `
    CREATE TABLE IF NOT EXISTS invitaciones (
        id INT PRIMARY KEY AUTO_INCREMENT,
        id_evento INT NOT NULL,
        uuid VARCHAR(15) NOT NULL,
        email_invitado VARCHAR(255) DEFAULT NULL,
        nombre_invitado VARCHAR(255) DEFAULT NULL,
        mesa_invitado INT DEFAULT NULL,
        silla_invitado INT DEFAULT NULL,
        escaneado INT DEFAULT 0,
        aceptada INT DEFAULT 0,
        estatus INT DEFAULT 1
    )
`;
const invitaciones_enviadas = `
    CREATE TABLE IF NOT EXISTS invitaciones_enviadas (
        id INT PRIMARY KEY AUTO_INCREMENT,
        uuid VARCHAR(15) NOT NULL,
        email_invitado VARCHAR(255) DEFAULT NULL,
        estatus INT DEFAULT 1
    )
`;

const usuarios = `
        CREATE TABLE IF NOT EXISTS users(
            id INT PRIMARY KEY AUTO_INCREMENT,
            username VARCHAR(75) DEFAULT NULL,
            PASSWORD TEXT NOT NULL
        )
`;

module.exports = {
    eventos,
    invitaciones,
    invitaciones_enviadas,
    usuarios
}