require('dotenv').config();
const express = require('express');
const router = express.Router();
const db = require('../model/tables');
const { v4: uuidv4 } = require('uuid');
const nodemailer = require("nodemailer");
const QRCode = require('qrcode');
const path = require('path');
const fs = require('fs');

var transport = nodemailer.createTransport({
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    auth: {
      user: process.env.MAIL_USER,
      pass: process.env.MAIL_PASS, 
    }
});

async function sendEmail(email, uuid) {
    const qrCodeDirectory = path.join(__dirname, '../qr_code/');
    const qrCodePath = path.join(qrCodeDirectory, uuid + '.png');
    try{
        QRCode.toFile(qrCodePath, uuid, {
            errorCorrectionLevel: 'H',
            width: 480,
            margin: 5
        }, async function(err) {
            if (err){
                console.log(err, "qr inside");
            };
            const logoCodeDirectory = path.join(__dirname, '../assets/');
            const logoPath = path.join(logoCodeDirectory, 'beh25.svg');
            const logoCodeBase64 = fs.readFileSync(logoPath, 'base64');
            
            const qrCodeBase64 = fs.readFileSync(qrCodePath, 'base64');
            

            const info = await transport.sendMail({
                from: '"Invitaciones digitales" <s.g@admin.com>',
                to: email,
                subject: "Invitación",
                html: `
                    <h4>Gracias por confirmas su asistencia</h4><br> 
                    <p>A continuación le brindarmeos un QR para presentar el dia del evento y acceder a sus lugares.</p><br>
                    <table>
                        <tr>
                            <td rowspan="3">
                                <img src="data:image/png;base64,${qrCodeBase64}"/>
                            </td>
                            <td>
                                <img src="data:image/svg+xml;base64,${logoCodeBase64}"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Para conocer los detalles del evento ingresa al siguiente link
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="https://www.grupobeh.com/25aniversario">htps://www.grupobeh.com/25aniversario</a>
                            </td>
                        </tr>
                    </table>
                `,
            });
        });
    }catch (err){
        console.log(err, "qr");
    }
}

async function sendEmailConfirmation(email, uuid) {
    try{
        const info = await transport.sendMail({
            from: '"Invitaciones digitales" <s.g@admin.com>',
            to: email,
            subject: "Confirmacion de invitación",
            html: `<h4>Ingrese al siguiente link para confirmar su asistencia al evento: </h4><br> 
            <a href="${process.env.VITE_HOST}/confirmar-asistencia/${uuid}">Confirmar asistencia</a>`,
        });
        let query = `
            INSERT INTO invitaciones_enviadas(uuid, email_invitado) 
            VALUES ('${uuid}', '${email}');
        `;
        db.query(query, (err, result) => { 
            if(err){
                console.log(err);
            }
        });
    } catch(err){
        res.status(500).send(err);
    }
}

router.post('/resend', async (req, res) => {
    const { uuid, email_invitado } = req.body.data;
    await sendEmailConfirmation(email_invitado, uuid);
    res.status(200).send({done: true});
});

router.post('/reset', (req, res) => {
    const { uuid } = req.body.data;
    const query = `
        UPDATE
            invitaciones
        SET
            aceptada = 0
        WHERE uuid = '${uuid}'
    `;

    db.query(query, (err, result) => {
        if(err){
            res.status(500).send(err);
        }
        res.status(200).send({'done': true});
    });
});

const usedUUIDs = [];
const generateUniqueUUID = () => {
    let uuid;
    do {
        uuid = uuidv4().replace(/-/g, '').substring(0, 15);
    } while (usedUUIDs.includes(uuid));

    usedUUIDs.push(uuid);
    return uuid;
};

router.get('/invitado/:uuid', (req, res) => {
    try{
        const { uuid } = req.params;
        const query = `SELECT invitaciones.*, eventos.nombre_evento AS evento FROM invitaciones INNER JOIN eventos ON eventos.id  = invitaciones.id_evento WHERE uuid = '${uuid}'`;
        db.query(query, (err, result) => { 
            if(err){
                console.log(err);
                res.status(500).send("ocurrio un problema")
            }else{
                res.status(200).send(result)
            }
        });
    } catch(err){
        res.status(500).send(err);
    }
});

router.post('/cancelar/:uuid', (req, res) => {
    const { uuid } = req.params;
    try{
        /**
         * 0 = pendiente
         * 1 = aceptada
         * 2 = cancelada
         */
        let query = `
            UPDATE invitaciones SET aceptada = 2 WHERE uuid = '${uuid}'
        `;
    
        db.query(query, (err, result) => {
            if(err){
                res.status(500).send(err);        
            }else{
                res.status(200).send({done: true});
            }
        });
    } catch(err){
        res.status(500).send(err);
    }
})

router.post('/aceptar/:uuid', (req, res) => {
    const { uuid } = req.params;
    const { email_invitado } = req.body.data 
    /**
     * 0 = pendiente
     * 1 = aceptada
     * 2 = cancelada
     */
    try{
        let query = `
            UPDATE invitaciones SET aceptada = 1 WHERE uuid = '${uuid}'
        `;
    
        db.query(query, (err, result) => {
            if(err){
                res.status(500).send(err);        
            }else{
                sendEmail(email_invitado, uuid);
                res.status(200).send({done: true});
            }
        });
    } catch(err){
        res.status(500).send(err);
    }

})

router.get('/invitados/:id', (req, res) => {
    const { id } = req.params;
    try{
        const query = `
            SELECT
                id, id_evento, uuid, email_invitado, 
                nombre_invitado, mesa_invitado,
                silla_invitado,
                CASE
                    WHEN aceptada = 0 THEN 'Pendiente'
                    WHEN aceptada = 1 THEN 'Aceptada'
                    ELSE 'Cancelada'
                END AS aceptada,
                estatus
            FROM invitaciones
            WHERE id_evento = ${id}
        `;
        db.query(query, (err, result) => { 
            if(err){
                console.log(err);
                res.status(500).send("ocurrio un problema")
            }else{
                res.status(200).send(result)
            }
        });
    } catch(err){
        res.status(500).send(err);
    }
});

router.post('/invitados', (req, res) => {
    const { evento, rows } = req.body.data;
    /**
     * 1.id_evento
     * 2.uuid
     * 3.email_invitado
     * 4.nombre_invitado
     * 5.mesa_invitado
     * 6.silla_invitado
     */
    try{
        rows.forEach((value, index) => {
            const uniqueUUID = generateUniqueUUID();
            const query = `
                INSERT INTO invitaciones(
                    id_evento,
                    uuid, 
                    email_invitado, 
                    nombre_invitado,
                    mesa_invitado,
                    silla_invitado
                ) values (
                    ${evento},
                    '${uniqueUUID}',
                    '${value[0]}',
                    '${value[1]}',
                    ${value[2]},
                    ${value[3]}
                );`;
            db.query(query, (err, result) => {
                if (err) {
                    res.status(500).send({
                        message: 'Error al insertar invitado en la fila ' + index
                    });
                }else{
                    sendEmailConfirmation(value[0], uniqueUUID);
                }
            });
        });
        res.status(200).send({
            done: true
        });
    } catch(err){
        res.status(500).send(err);
    }
});

router.get('/mesa/:uuid', (req, res) => {
    try{
        const { uuid } = req.params;
        const query = `
            SELECT 
                i.id_evento,
                i.nombre_invitado,
                i.mesa_invitado, 
                i.silla_invitado, 
                e.numero_sillas_x_mesa, e.numero_mesas
            FROM invitaciones i
            INNER JOIN eventos e ON e.id = i.id_evento
            WHERE uuid = '${uuid}'
        `;
        const actualizar_escaneado = `
            UPDATE
                invitaciones
            SET escaneado = 1
            WHERE uuid = '${uuid}';
        `;

        db.query(query, (err, result) => { 
            if(err){
                console.log(err);
                res.status(500).send("ocurrio un problema")
            }else{
                db.query(actualizar_escaneado, (er, result) => {
                    if(err)
                        res.status(500).send("ocurrio un problema al actualizar escaneado")
                })
                const {id_evento, mesa_invitado, silla_invitado,  numero_sillas_x_mesa, numero_mesas} = result[0];
                let q = `
                    SELECT
                        id, id_evento, uuid, nombre_invitado, 
                        mesa_invitado, silla_invitado,
                        CONCAT_WS('.', invitaciones.mesa_invitado, invitaciones.silla_invitado) AS convinacion, 
                        CASE 
                        WHEN aceptada = 0 THEN 'pendiente'
                            WHEN aceptada = 1 THEN 'aceptada'
                            WHEN aceptada = 2 THEN 'cancelada'
                        END as aceptada 
                    FROM invitaciones
                    WHERE
                        id_evento = ${id_evento} AND 
                        uuid <> '${uuid}' AND
                        escaneado = 1
                    ORDER BY mesa_invitado, silla_invitado 
                `;
                
                let data_user = result[0];
                data_user["id_html"] = `${mesa_invitado}.${silla_invitado}`                
                db.query(q, (err, result) =>{
                    if(err){
                        console.log(err);
                        res.status(500).send("ocurrio un problema");
                    }
                    res.status(200).send({'data': result, 'user': data_user});
                });
            }
        });
    } catch(err){
        res.status(500).send(err);
    }
});

module.exports = router;