const express = require('express');
const router = express.Router();
const db = require('../model/tables');

router.post('/eventos', (req, res) => {
    const { id_evento, nombre_evento, numero_mesas, numero_sillas_x_mesa } = req.body.data;
    try{
        let query = '';
        if(id_evento){
            query = `
                UPDATE eventos
                SET
                    nombre_evento = '${nombre_evento}',
                    numero_mesas = ${numero_mesas},
                    numero_sillas_x_mesa = ${numero_sillas_x_mesa}
                WHERE
                    id = ${id_evento};`;
        }else{
            query = `
                INSERT INTO eventos (nombre_evento, numero_mesas, numero_sillas_x_mesa)
                VALUES('${nombre_evento}', ${numero_mesas}, ${numero_sillas_x_mesa});
            `;
        }
        db.query(query, (err, result) => {
            if(err){
                res.status(500).send(err);
            }else{
                res.status(200).send({done: true});
            }
        });
    }catch(err){
        res.status(500).send(err);
    }
});

router.delete('/eventos/:id', (req, res) => {
    const { id } = req.params;
    try{
        let query = `
            DELETE FROM eventos WHERE id = ${id};
        `;
    
        db.query(query, (err, result) => {
            if(err){
                res.status(500).send(err);
            }else{
                query = `
                    DELETE FROM invitaciones WHERE id_evento = ${id}
                `;
                db.query(query, (err, result) => {
                    if(err){
                        res.status(500).send(err);
                    }
                    res.status(200).send({ done : true });
                });
            }
        });
    } catch(err){
        res.status(500).send(err);
    }
});

router.get('/eventos', (req, res) => {
    try{
        let query = `
            SELECT *
            FROM eventos 
            WHERE estatus = 1;
        `;
        db.query(query, (err, result) => {
            if(err){
                res.status(500).send(err);
            }else{
                res.status(200).send(result);
            }
        });
    } catch(err){
        res.status(500).send(err);
    }
});

module.exports = router;