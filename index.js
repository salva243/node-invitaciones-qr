const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const express = require('express');
const path = require('path');
const db = require('./model/tables');
const cors = require('cors');
const eventos = require('./controladores/eventos');
const invitados = require('./controladores/invitados');

const q_eventos = require('./querys/migraciones').eventos;
const q_invitaciones = require('./querys/migraciones').invitaciones;
const q_invitaciones_enviadas = require('./querys/migraciones').invitaciones_enviadas;
const usuarios = require('./querys/migraciones').usuarios;

const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json());
app.use(cors());
app.use(invitados);
app.use(eventos);

app.post('/login', (req, res) => {
  const { user, password } = req.body.data;
  let _contrasena = password;
  const query = `
    SELECT username, password FROM users WHERE username = '${user}'
  `;

  db.query(query, (err, result) => {
    if(err){
      res.status(500).send(err);
    }
    let _password = result[0].password;

    const isPasswordValid = bcrypt.compareSync(_contrasena, _password);

    if(isPasswordValid){
      const token = jwt.sign(
        {
         password: _password,
         user: user
        }, "ayBwtrygHUtUJwMKNxTdbgQYyvKUBaFKmTEqgjPzeHwgzXDwGm", 
        {
          expiresIn: '2h',}
      );
      res.status(200).send({token: token});
    } else {
      res.status(500).send(err);
    }

  });
});

app.post('/validate', (req, res) => {
  const { token } = req.body.data;
  const secretKey = 'ayBwtrygHUtUJwMKNxTdbgQYyvKUBaFKmTEqgjPzeHwgzXDwGm';
  jwt.verify(token, secretKey, (error, decodedToken) => {
    if (error) {
      res.status(500).send(false);
    } else {
      res.status(200).send(true);
    }
  });
});

app.get('/create-admin', async (req, res) => {
  const username = 'beh_admin_25';
  const plainPassword = 'beh_25_log';

  try {
    const hashedPassword = await bcrypt.hash(plainPassword, 10);
    const query = `
      INSERT INTO users (username, password) 
      VALUES ('${username}', '${hashedPassword}')
    `;
    
    db.query(query, (err, result) => {
      if (err) {
        res.status(500).send(err);
      } else {
        res.status(200).send({ done: true });
      }
    });
  } catch (error) {
    res.status(500).send(error);
  }
});


app.get('/', (req, res) => {
  res.status(200).send('Ruta home');
});

app.listen(PORT, () => {
  db.query(q_eventos, (err, result) => {
    if (err) throw err;
  });
  db.query(q_invitaciones_enviadas, (err, result) => {
    if (err) throw err;
  });
  db.query(q_invitaciones, (err, result) => {
    if (err) throw err;
  });
  db.query(usuarios, (err, result) => {
    if (err) throw err;
  });

  console.log(`Servidor escuchando en el puerto ${PORT}`);
});
